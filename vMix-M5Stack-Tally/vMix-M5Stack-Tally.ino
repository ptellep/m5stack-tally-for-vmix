
#include <WiFi.h>
#include <WiFiClient.h>
#include <M5Stack.h>
#include <Preferences.h>
#include <PinButton.h>
#include <WebServer.h>
#include <stdio.h>
#include <string>  

Preferences preferences;

const char* ssid =  "vMix-M5Stack-Tally";
const char* password = "";

String header;

String WIFI_SSID;
String WIFI_PASS;
String VMIX_IP = "192.168.1.69";
int VMIX_PORT = 8099;
int TALLY_NR = 1;
int new_tally;
int battery_loop = 0;
int battery_level = 100;
int raw_battery;
// END CONFIGURATION

WiFiClient client;
WebServer server(80);  // Object of WebServer(HTTP port, 80 is default)

// Configure Buttons
PinButton btnC(37);
PinButton btnB(38);
PinButton btnA(39);

char currentState = -1;
char screen = 0;

void setup()
{
  Serial.begin(115200);

  setCpuFrequencyMhz(80);

  M5.begin();
  Wire.begin();
  delay(10);
  M5.Lcd.setRotation(1);
  M5.Lcd.setTextSize(3);
  M5.Lcd.setCursor(45, 30);
  M5.Lcd.println("vMix M5Stack");
  M5.Lcd.setCursor(30, 60);
  M5.Lcd.println("Wireless Tally");
  M5.Lcd.setTextSize(2);
  M5.Lcd.setCursor(50, 110);
  M5.Lcd.println("by Peter Tellep");
  M5.Lcd.setCursor(40, 160);
  M5.Lcd.println("based on M5Stick-C");
  M5.Lcd.println(" Tally by Guido Visser");
  delay(2000);
  M5.Lcd.println();
  M5.Lcd.print("   LOADING.");
  for(int x=0; x < 7; x++){
    delay(1000);
    M5.Lcd.print(".");
  }
  Serial.println("CPU:");
  Serial.println(getCpuFrequencyMhz());

  cls();
  
  preferences.begin("vMixTally", false);

  Serial.println("SSID from preferences");
  Serial.println(preferences.getString("wifi_ssid").length());

  if(preferences.getString("wifi_ssid").length() > 0) {
    WIFI_SSID = preferences.getString("wifi_ssid");
    WIFI_PASS = preferences.getString("wifi_pass");
    startWiFi();
  } else {
    startLocalWiFi();
  }
 
  TALLY_NR = preferences.getUInt("tally");
  VMIX_IP = preferences.getString("vmix_ip");
    
  preferences.end();

}

void drawBattery(){
  M5.Lcd.drawRect(280,8,26,14,WHITE);
  M5.Lcd.drawRect(305,11,4,8,WHITE);
  switch(battery_level){
    case 100: M5.Lcd.fillRect(282,10,22,10,WHITE);
    case 75: M5.Lcd.fillRect(282,10,17,10,WHITE);
    case 50: M5.Lcd.fillRect(282,10,12,10,WHITE);
    case 25: M5.Lcd.fillRect(282,10,7,10,WHITE);
    case 0: M5.Lcd.fillRect(282,10,2,10,WHITE);
  }
  //M5.Lcd.setCursor(270,25);
  //M5.Lcd.print(battery_level);
  //M5.Lcd.setCursor(270,35);
  //M5.Lcd.print(raw_battery);
  
}

//void connectTovMix(){
//    if ((VMIX_IP != "")){ // && (wifi_connected == true)) {
//      connectTovMix();
//    } else {
//      showSettingsScreen();
//    }
//  
//}

void loadWiFiPreferences(){
  Serial.println("load preferences");
  String wifi_ssid;
  String wifi_pass;
  
  preferences.begin("vMixTally", false);
  
  wifi_ssid = preferences.getString("wifi_ssid");
  wifi_pass = preferences.getString("wifi_pass");
  preferences.end();
}
void loadvMixPreferences(){
  int tally_nr;
  String vmix_ip;
  
  preferences.begin("vMixTally", false);

  tally_nr = preferences.getUInt("tally");
  vmix_ip = preferences.getString("vmix_ip");
  preferences.end();  
}

void saveWiFiPreferences(String wifi_ssid, String wifi_pass){
  preferences.begin("vMixTally", false);
  if(wifi_ssid != ""){
    preferences.putString("wifi_ssid", &(WIFI_SSID[0]));
    preferences.putString("wifi_pass", &(WIFI_PASS[0]));
  }
  preferences.end();
}
void savevMixPreferences(){
//  preferences.begin("vMixTally", false);
//  if(tally != "") {
//    TALLY_NR =  std::atoi(tally.c_str());  
//    preferences.putUInt("tally", TALLY_NR);
//  }
//  if(server.arg("ssid") != ""){
//    WIFI_SSID = server.arg("ssid");  
//    WIFI_PASS = server.arg("pwd");
//    preferences.putString("wifi_ssid", &(WIFI_SSID[0]));
//    preferences.putString("wifi_pass", &(WIFI_PASS[0]));
//  }
//  if(server.arg("vmixip") != ""){
//    VMIX_IP = server.arg("vmixip");  
//    preferences.putString("vmix_ip", &(VMIX_IP[0]));
//  }
//  preferences.end();
}
void startServer(){
    server.on("/", handle_root);
    server.on("/save", handle_save);
    server.on("/vmix_settings", handle_vmix_settings);
    server.on("/save_vmix_settings", handle_save_vmix_settings);  
    server.begin();
    //serverap.begin();
    Serial.println("HTTP server started");
    Serial.println("Connect to WiFi");
    //Serial.println((String)wifi_connected);
    delay(100);
}
void startWiFi(){
    WiFi.mode(WIFI_STA);
    WiFi.begin(&(WIFI_SSID[0]), &(WIFI_PASS[0]));
    
    // We start by connecting to a WiFi network
    
    //WiFi.begin(&(WIFI_SSID[0]), &(WIFI_PASS[0]));
    //WiFi.softAP(ssid, password);
  
    M5.Lcd.println();
    M5.Lcd.setCursor(25, 60);
    M5.Lcd.println("Waiting for WiFi...");
  
    //while (WiFi.status() != WL_CONNECTED) {
    int tries = 0;
    boolean wifi_connected = true;
    
    while(WiFi.waitForConnectResult() != WL_CONNECTED){
      M5.Lcd.print(".");
      delay(1000);
      tries++;
      if(tries > 10){
        tries = 0;
        Serial.println("Wifi connection failed, start local wifi");
        wifi_connected = false;
        startLocalWiFi();
        break;        
      }
    } 
    
    if(wifi_connected == false){
      startLocalWiFi();
    } else {
      cls();
      M5.Lcd.println("WiFi connected");
      M5.Lcd.println("IP address: ");
      M5.Lcd.print(WiFi.localIP());
      startServer();
      connectTovMix();     
    }
}

// This starts the M5Stack as a WiFi Access Point so you can configure it 
void startLocalWiFi() {
  WiFi.mode(WIFI_AP);
  WiFi.softAP("vMix-M5Stack-Tally", "12345678");
  showAPScreen();
  startServer();
}

// Clear LCD Screen
void cls()
{
  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setCursor(0, 0);
  
}

// Connect to vMix instance
boolean connectTovMix()
{
  cls();
  M5.Lcd.println("Connecting to vMix...");

  if (client.connect(&(VMIX_IP[0]),VMIX_PORT))
  {
    M5.Lcd.println("Connected to vMix!");
    Serial.println("------------");

    // Subscribe to the tally events
    client.println("SUBSCRIBE TALLY");
    showTallyScreen();
    return true;
  }
  else
  {
    char tryCount = 0;
    cls();
    M5.Lcd.println("Could not connect to vMix");
    M5.Lcd.println("Retrying: 0/3");
    boolean retry = false;
    for (int i = 0; i < 3; i++)
    {
      Serial.print(i);
      retry = retryConnectionvMix(i);
      if (!retry) {
        return true;
      }
    }
    cls();
    M5.Lcd.println("Couldn't connect to vMix");
    M5.Lcd.println();
    M5.Lcd.println("Please restart device");
    M5.Lcd.println("to retry");
    return false;
  }
}

boolean retryConnectionvMix(char tryCount) {
  cls();
  M5.Lcd.println("Couldn't connect to vMix");
  M5.Lcd.print("Retrying: ");
  M5.Lcd.print(tryCount);
  M5.Lcd.print("/3");
  delay(2000);
  boolean conn = connectTovMix();
  if (conn) {
    return false;
  }
  return true;
}

// Handle Tally State
void displayTallyState(uint16_t bgcolor, uint16_t color, int x, int y, String state){
  M5.Lcd.fillScreen(bgcolor);
  M5.Lcd.setTextColor(color, bgcolor);
  M5.Lcd.setCursor(x, y);
  M5.Lcd.println(state);
}  

// Handle incoming data
void handleData(String data)
{
  // Check if server data is tally data
  if (data.indexOf("TALLY") == 0)
  {

    char newState = data.charAt(TALLY_NR + 9);
    // Check if tally state has changed
    if (currentState != newState || screen == 1)
    {
      currentState = newState;
      showTallyScreen();
    }
  }
  else
  {
    Serial.print("Response from vMix: ");
    Serial.println(data);
  }
}

void showTallyScreen() {
  cls();
  screen = 0;
  M5.Lcd.setTextSize(10);
  switch (currentState)
  {
    case '0':
      displayTallyState(BLACK,WHITE,73,95,"SAFE");
      break;
    case '1':
      displayTallyState(RED,WHITE,75,95,"LIVE");
      break;
    case '2':
      displayTallyState(GREEN,BLACK,90,95,"PRE");
      break;
    default:
      displayTallyState(BLACK,WHITE,73,95,"SAFE");
  }
  M5.Lcd.setTextSize(2);
  M5.Lcd.setCursor(10,10);
  String camera = "CAMERA: " + (String)(TALLY_NR +1);
  M5.Lcd.println(camera);
  drawBattery();


}

void showMsg(const char* msg){
  cls();
  M5.Lcd.setTextSize(1);
  M5.Lcd.setTextColor(WHITE,BLACK);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.println(msg);
}

void showTallyNum(String msg){
  cls();
  M5.Lcd.setTextSize(5);
  M5.Lcd.setTextColor(WHITE,BLACK);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.println(msg);
}

// Show the current network settings
void showNetworkScreen() {
  Serial.println("Showing Network screen");
  screen = 1;
  cls();
  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setTextSize(2);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.println();
  M5.Lcd.print("SSID:");
  M5.Lcd.print(WIFI_SSID);
  M5.Lcd.println();
  M5.Lcd.print("IP Address:");
  M5.Lcd.print(WiFi.localIP());
  M5.Lcd.println();
  M5.Lcd.print("Tally Num: ");
  M5.Lcd.print(TALLY_NR);
  M5.Lcd.println();
  M5.Lcd.print("Camera Num: ");
  M5.Lcd.print(TALLY_NR + 1);
}

void showSettingsScreen() {
  Serial.println("Showing Settings screen");
  screen = 2;
  cls();

  preferences.begin("vMixTally", false);
  
  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setTextSize(2);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.println("Settings");
  
  Serial.println("Tally");
  Serial.println(preferences.getUInt("tally"));
  M5.Lcd.println();
  M5.Lcd.println();
  M5.Lcd.print("CAMERA:");
  M5.Lcd.print(preferences.getUInt("tally")+1);  
  M5.Lcd.println();
  M5.Lcd.print("TALLY:");
  M5.Lcd.print(preferences.getUInt("tally"));  
  
  Serial.println("SSID");
  Serial.println(preferences.getString("wifi_ssid"));
  M5.Lcd.println();
  M5.Lcd.print("SSID:"); 
  M5.Lcd.print(preferences.getString("wifi_ssid"));  
  
  Serial.println("PWD");
  Serial.println(preferences.getString("wifi_pass"));
  M5.Lcd.println();
  M5.Lcd.print("PWD:");
  M5.Lcd.print(preferences.getString("wifi_pass"));  
  
  Serial.println("vMix IP");
  Serial.println(preferences.getString("vmix_ip"));
  M5.Lcd.println();
  M5.Lcd.println();
  M5.Lcd.print("vMix IP:");
  M5.Lcd.print(preferences.getString("vmix_ip"));  
  
  M5.Lcd.println();
  preferences.end();
}

void showAPScreen() {
  Serial.println("Showing Access Point Screen");
  screen = 0;
  cls();

  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setTextSize(2);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.setCursor(10,20);
  M5.Lcd.println("Unable to connect to WiFi");
  M5.Lcd.println();
  M5.Lcd.println("Please connect to:");
  M5.Lcd.println();
  M5.Lcd.println("SSID: vMix-M5tack-Tally");
  M5.Lcd.println("Pwd: 12345678");
  M5.Lcd.println();
  M5.Lcd.println("Open http://192.168.4.1 \n in a browser and \n configure your WiFi");
  M5.Lcd.println();
}

void showTallySetScreen() {
  Serial.println("Showing Tally Set Screen");
  screen = 5;
  cls();

  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setTextSize(2);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.setCursor(20,20);
  M5.Lcd.print("Current Camera: ");
  M5.Lcd.print(TALLY_NR + 1);
  M5.Lcd.setCursor(20,60);
  M5.Lcd.print("New Camera: ");
  M5.Lcd.print(new_tally + 1);
  M5.Lcd.setCursor(40, 220);
  M5.Lcd.print("SAVE");
  M5.Lcd.setCursor(155, 220);
  M5.Lcd.print("-");
  M5.Lcd.setCursor(250, 220);
  M5.Lcd.print("+");

}


// WEBSERVER STUFF
String HEADER =  "<!DOCTYPE html>\
  <html lang='en'>\
  <head>\
  <meta charset='UTF-8'>\
  <meta name='viewport' content='width=device-width, initial-scale=1.0, shrink-to-fit=no'>\
  <title>vMix M5Stick-C Tally</title>\
  <link rel='stylesheet' type='text/css' href='style.css'>\
  <style>\
  .wrapper,input{width:100%}body,html{padding:0;margin:0}.wrapper{padding:10px;box-sizing:border-box}.wrapper h1{text-align:center}input[type=submit]{width:50px;margin:10px auto}\
  </style>\
  </head>\
  <body>\
  <div class='wrapper'>";

String FOOTER = "</div>\
  </body>\
  </html>";
  
void handle_root() {
  String tally = (String)TALLY_NR;
  String HTML = HEADER;
  HTML += "<div class='wrapper'>";
  HTML += "<h1>vMix M5Stack Tally Settings</h1>";
  HTML += "<form action='/save' method='post'>";
  HTML += "SSID:<br/>";
  HTML += "<input type='text' name='ssid' value='" + (String)WIFI_SSID + "'>";
  HTML += "Password:<br/>";
  HTML += "<input type='text' name='pwd' value='" + (String)WIFI_PASS + "'>";
  HTML += "vMix IP Address:<br/>";
  HTML += "<input type='text' name='vmixip' value ='" + (String)VMIX_IP + "'>";
  HTML += "Tally Number:<br/>";
  HTML += "<input type='number' name='tally_num' value='" + tally + "'>";
  HTML += "<input type='submit' value='SAVE' class='btn btn-primary'>";
  HTML += "</form>";
  HTML += FOOTER;
  
  server.send(200, "text/html", HTML);
}

void handle_vmix_settings() {
  String tally = (String)TALLY_NR;
  String HTML = HEADER;
  HTML += "<div class='wrapper'>";
  HTML += "<h1>vMix M5Stack Tally Settings</h1>";
  HTML += "<form action='/save_vmix_settings' method='post'>";
  HTML += "vMix IP Address:<br/>";
  HTML += "<input type='text' name='vmixip' value ='" + (String)VMIX_IP + "'>";
  HTML += "Tally Number:<br/>";
  HTML += "<input type='number' name='tally_num' value='" + tally + "'>";
  HTML += "<input type='submit' value='SAVE' class='btn btn-primary'>";
  HTML += "</form>";
  HTML += FOOTER;
  
  server.send(200, "text/html", HTML);
  
}

void handle_save() {
  String message = "";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  message += " direct:" + server.arg("tally_num");
  server.send(404, "text/plain", message);
  Serial.println(message);

  
  showTallyNum(server.arg("tally_num"));
  String tally = server.arg("tally_num");

  // save value in preferences
  preferences.begin("vMixTally", false);
  if(tally != "") {
    TALLY_NR =  std::atoi(tally.c_str());  
    preferences.putUInt("tally", TALLY_NR);
  }
  if(server.arg("ssid") != ""){
    WIFI_SSID = server.arg("ssid");  
    WIFI_PASS = server.arg("pwd");
    preferences.putString("wifi_ssid", &(WIFI_SSID[0]));
    preferences.putString("wifi_pass", &(WIFI_PASS[0]));
  }
  if(server.arg("vmixip") != ""){
    VMIX_IP = server.arg("vmixip");  
    preferences.putString("vmix_ip", &(VMIX_IP[0]));
  }
  preferences.end();
  
  // We start by connecting to a WiFi network
  M5.Lcd.setTextSize(2);
  M5.Lcd.println(WIFI_SSID);
  M5.Lcd.println(WIFI_PASS);
  delay(2000);
  
 // WiFi.begin(&(WIFI_SSID[0]), &(WIFI_PASS[0]));

  M5.Lcd.println();
  M5.Lcd.setCursor(25, 60);
  M5.Lcd.println("Waiting for WiFi...");

  int tries = 0;
  while (WiFi.status() != WL_CONNECTED) {
    M5.Lcd.print(".");
    delay(1000);
    tries++;
    if(tries > 10){
      tries = 0;
      break;
    }
  }
  cls();
  M5.Lcd.println("WiFi connected");
  M5.Lcd.println("IP address: ");
  M5.Lcd.print(WiFi.localIP());


  delay(2000);
  connectTovMix();
  //server.send(200, "text/html", RESP);
}

void handle_save_vmix_settings() {
  String message = "";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  message += " direct:" + server.arg("tally_num");
  server.send(404, "text/plain", message);
  Serial.println(message);

  showTallyNum(server.arg("tally_num"));
  String tally = server.arg("tally_num");

  // save value in preferences
  preferences.begin("vMixTally", false);
  if(tally != "") {
    TALLY_NR =  std::atoi(tally.c_str());  
    preferences.putUInt("tally", TALLY_NR);
  }
  if(server.arg("vmixip") != ""){
    VMIX_IP = server.arg("vmixip");  
    preferences.putString("vmix_ip", &(VMIX_IP[0]));
  }
  preferences.end();
  startWiFi();  
}

void updateTallyNR(int tally){
  preferences.begin("vMixTally", false);
  if(tally >= 0) {
    TALLY_NR =  tally;  
    preferences.putUInt("tally", TALLY_NR);
  }
  preferences.end();
  connectTovMix(); 
}

int8_t getBatteryLevel()
{
  Wire.beginTransmission(0x75);
  Wire.write(0x78);
  if (Wire.endTransmission(false) == 0
   && Wire.requestFrom(0x75, 1)) {
    switch (Wire.read() & 0xF0) {
    case 0xE0: return 25;
    case 0xC0: return 50;
    case 0x80: return 75;
    case 0x00: return 100;
    default: return 0;
    }
    raw_battery = Wire.read();
  }
  return -1;
}

void loop()
{
  
  server.handleClient();

  battery_loop++;
  if(battery_loop > 1000000) {
    battery_level = getBatteryLevel();
    battery_loop = 0;
    Serial.println(battery_level);
    drawBattery();
  }
  
  
  btnC.update();
  btnB.update();
  btnA.update();
  if (btnC.isClick()) {
    if (screen == 0) {
      showNetworkScreen();
    } else if (screen == 1) {
      showTallyScreen();
    } else if (screen == 5) {
      new_tally++;
      showTallySetScreen();
    }
  }
  if (btnB.isClick()) {
    if (screen == 0) {
      showSettingsScreen();
    } else if (screen == 2) {
      showTallyScreen();
    } else if (screen == 5) {
      new_tally--;
      if(new_tally < 0){
        new_tally = 0;
      }
      showTallySetScreen();
    }
  }
  if (btnA.isClick()) {
//    preferences.begin("vMixTally", false);
//    preferences.clear();
//    preferences.end();
    if (screen == 0) {
      new_tally = TALLY_NR;
      showTallySetScreen();
    } else if (screen == 5) {
      updateTallyNR(new_tally);
      showTallyScreen();
    } 
  }
//  if (btnA.wasPressed(600)){
//    M5.Lcd.println("600!");
//  
//  }

  while (client.available())
  {
    String data = client.readStringUntil('\r\n');
    handleData(data);
  }

}
